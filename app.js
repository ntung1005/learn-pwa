const apiKey ='d3a0b0faa9e341149d21cc300423f2ac'; //Key newsapi.org
const main = document.getElementById('main');
const sourceSelector= document.getElementById('sourceSelector');

const defaultSource = '';

window.addEventListener('load',async (e)=>{
    await updateNews();
    await updateSources();
    sourceSelector.value=defaultSource;

    sourceSelector.addEventListener('change',(e)=>{
        updateNews(e.target.value)
    });

    if('serviceWorker' in navigator){
        try{
            navigator.serviceWorker.register('sw.js');
            console.log(`SW registered`);
        } catch (error) {
            console.log(`SW reg failed`);
        }
    }
});

async function updateSources(){
    const res = await fetch(`https://newsapi.org/v2/sources?apiKey=${apiKey}`);
    const json = await res.json();

    sourceSelector.innerHTML=(`<option value=''>Sap xep</option>`+json.sources.map((src)=>`<option value=${src.id}>${src.name}</option>`).join('\n'))
}

async function updateNews(source=defaultSource){
    const res = await fetch(`https://newsapi.org/v2/everything?sources=${source}&q=bitcoin&apiKey=${apiKey}`);
    const json = await res.json();
    main.innerHTML = json.articles.map(createArticle).join('\n');
}

function createArticle(article) {
    return `
        <div class="article">
            <a href="${article.url}">
                <h2>${article.title}</h2>
                <img src="${article.urlToImage}"/>
                <p>${article.description}</p>
            </a>
        </div>
    `
}
